#include<stdio.h>
#include<math.h>


int time_service_begins_calculate(int start_time, int arrival_time, int ck, int prev_time_service_ends)
{
    int time_service_begins;

    if(ck == 0)
        time_service_begins = start_time + arrival_time;

    else if(arrival_time > prev_time_service_ends)
        time_service_begins = arrival_time;

    else if(arrival_time < prev_time_service_ends)
        time_service_begins = prev_time_service_ends;

    return time_service_begins;
}


int time_customer_waits_in_queue_calculate(int arrival_time, int time_service_begins)
{
    int time_customer_waits_in_queue = arrival_time - time_service_begins;

    if(time_customer_waits_in_queue <0)
        time_customer_waits_in_queue = -1 * time_customer_waits_in_queue;

    return time_customer_waits_in_queue;
}


int time_service_ends_calculate(int service_time_begins, int service_time)
{
    int service_time_ends = service_time_begins + service_time;

    return service_time_ends;
}


int time_customer_spends_in_system_calculate(int time_customer_waits_in_queue, int service_time)
{
    int time_customer_spends_in_system = time_customer_waits_in_queue + service_time;

    return time_customer_spends_in_system;
}

int idle_time_of_server_calculate(int service_time_ends, int time_service_begins, int ck, int prev_service_time_ends)
{
    if(ck == 0)
        service_time_ends = 0;
    else
        service_time_ends = prev_service_time_ends;

    int idle_time_of_server = service_time_ends - time_service_begins;

    if(idle_time_of_server < 0)
        idle_time_of_server = -1 * idle_time_of_server;

    return idle_time_of_server;
}


int main()
{
    printf("Please Enter The Number of Customer: ");
    int customer_number;
    scanf("%d", &customer_number);

    printf("Please Enter The Arrival Times: ");
    int arrival_time[customer_number];

    int n;

    for(n=0;n<customer_number;++n)
        scanf("%d", &arrival_time[n]);

    printf("Please Enter Service Times: ");
    int service_time[customer_number];

    for(n=0;n<customer_number;++n)
        scanf("%d", &service_time[n]);

    printf("Please Enter Start Time: ");
    int start_time;

    scanf("%d", &start_time);

    printf("Please Enter Total Simulation Run Time: ");
    int run_time;

    scanf("%d", &run_time);

    int time_service_begins;
    int time_service_ends;
    int i;
    int count = 0;
    int check = 0;
    int total_waiting_time = 0;
    int total_server_idle_time = 0;
    int total_service_time = 0;
    float avg_wtng_time;
    int arr[customer_number][8];

    printf("-----------------------------------------------------------------------------------------------------------------------\n");
    printf("                                           SIMULATION OF AN ATM BOOTH\n");



    printf("-----------------------------------------------------------------------------------------------------------------------\n");
    printf("    A         B          C             D               E              F                  G                  H\n");
    printf("Customer||");
    printf("Arr. Time||");
    printf("Ser. Time||");
    printf("Time Ser. Begins||");
    printf("Cust. Waits||");
    printf("Time Ser. Ends||");
    printf("Cust. Spends in Sys.||");
    printf("Server's Idle Time");
    printf("\n");
    printf("-----------------------------------------------------------------------------------------------------------------------\n");


    for(i=0;i<customer_number;++i){
        arr[i][0]= i+1;

        if(arr[i][0]<10)
            printf("    %d   ||", arr[i][0]);
        else
            printf("    %d  ||", arr[i][0]);

        arr[i][1]= arrival_time[i];

        if(arr[i][1]<10)
            printf("    %d    ||", arr[i][1]);
        else
            printf("    %d   ||", arr[i][1]);

        arr[i][2]= service_time[i];
        if(arr[i][2]<10)
            printf("    %d    ||", arr[i][2]);
        else
            printf("    %d   ||", arr[i][2]);

        arr[i][3] = time_service_begins_calculate(start_time, arr[i][1], i, arr[i-1][5]);
        if(arr[i][3]<10)
            printf("       %d        ||", arr[i][3]);
        else
            printf("       %d       ||", arr[i][3]);

        arr[i][4] = time_customer_waits_in_queue_calculate(arr[i][1], arr[i][3]);
        if(arr[i][4]<10)
            printf("     %d     ||", arr[i][4]);
        else
            printf("     %d    ||", arr[i][4]);

        arr[i][5] = time_service_ends_calculate(arr[i][3], arr[i][2]);
        if(arr[i][5]<10)
            printf("      %d       ||", arr[i][5]);
        else
            printf("      %d      ||", arr[i][5]);


        arr[i][6] = time_customer_spends_in_system_calculate(arr[i][4], arr[i][2]);
        if(arr[i][6]<10)
            printf("         %d          ||", arr[i][6]);
        else
            printf("         %d         ||", arr[i][6]);

        arr[i][7] = idle_time_of_server_calculate(arr[i][5], arr[i][3], i, arr[i-1][5]);
        printf("        %d", arr[i][7]);
        printf("\n");

    }




    for(i=0;i<customer_number;++i){

        arr[i][0]= i+1;


        arr[i][1]= arrival_time[i];


        arr[i][2]= service_time[i];


        arr[i][3] = time_service_begins_calculate(start_time, arr[i][1], i, arr[i-1][5]);


        arr[i][4] = time_customer_waits_in_queue_calculate(arr[i][1], arr[i][3]);


        arr[i][5] = time_service_ends_calculate(arr[i][3], arr[i][2]);



        arr[i][6] = time_customer_spends_in_system_calculate(arr[i][4], arr[i][2]);


        arr[i][7] = idle_time_of_server_calculate(arr[i][5], arr[i][3], i, arr[i-1][5]);


    }



    printf("-----------------------------------------------------------------------------------------------------------------------\n");

    printf("\n\n-----------------------------------------------------ASUMPTION---------------------------------------------------------\n");

    for(i=0;i<customer_number;++i){
        total_waiting_time += arr[i][4];
    }
    printf("Average Waiting Time: %.2f\n", (float)total_waiting_time/(float)customer_number);


    for(i=0;i<customer_number;++i){
        if(arr[i][4])
            ++count;
    }
    printf("Probability of a customer to wait: %.2f\n", (float)count/(float)customer_number);
    count = 0;

    for(i=0;i<customer_number;++i){
        total_server_idle_time += arr[i][7];
    }
    printf("Probability of idle server: %.2f\n", (float)total_server_idle_time/(float)arr[customer_number-1][5]);

    for(i=0;i<customer_number;++i){
        total_service_time += arr[i][2];
    }
    printf("Average Service Time: %.2f\n", (float)total_service_time/(float)customer_number);

    for(i=0;i<customer_number;++i){
        if(arr[i][5]<=run_time)
            ++check;
    }

    printf("Max customer output: %d\n", check);

    printf("Total Process Time: %d\n", arr[check-1][5]);





    return 0;
}
